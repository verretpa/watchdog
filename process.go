package main

import (
	"fmt"

	"github.com/shirou/gopsutil/load"
	"github.com/shirou/gopsutil/process"
	"reflect"
)

func main() {

	avg, _ := load.Avg()
	pids, _ := process.Pids()

	fmt.Println(avg.Load1)
	fmt.Printf("%s\n", reflect.TypeOf(avg.Load1))

	for _, pid := range pids {
		//fmt.Printf("Processen %s\n", pid)

		pro, _ := process.NewProcess(pid)
		na, _ := pro.Name()
		cpp, _ := pro.CPUPercent()

		//fmt.Printf("%s:\t\t%2.2f\n", na, cpp)

		if cpp > 1 {
			fmt.Printf("Groter, %s:\t\t%2.2f\n", na, cpp)
		}
	}

	a := 91
	b := 90

	if a > 90 {
		fmt.Printf("groter")
	}
	fmt.Println(a, b)
}
